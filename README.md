# Filter Twig

This is a very simple module to make twig values available as an input filter.
When editing a form where this text format is used in a field, you can type
twig that will be replaced when the filed is rendered.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/filter_twig).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/filter_twig).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


# Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

1. Visit the text format administration page at: `/admin/config/content/formats`
2. Check the 'Replaces Twig values' filter and save the text format.
3. When editing a form where this text format is used in a field, you can type
   twig that will be replaced when the field is rendered.


## Maintainers

- Andriy Malyeyev - [slivorezka](https://www.drupal.org/u/slivorezka)
